<?php

$formulario=<<<EOFORM
		
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Comprobación de nombres (Formulario). Comprobación de nombres. PHP.
  Bartolomé Sintes Marco</title>
  <meta name="generator" content="amaya 8.7.1, see http://www.w3.org/Amaya/" />
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>

<form action="formularioUsuario.php" method="get">
  <h1>Comprobación de nombres</h1>
  <fieldset>
    <legend>Formulario</legend>    
	<label for=nombre>Nombre:</label>
    <input type="text" name="nombre" id="nombre" /></br>
	
	<label for=edad>Edad:</label>
    <input type="text" name="edad" id="edad" /></br>

	<label for=email>Email:</label>
    <input type="text" name="email" id="email" />	</br>

	<label for=sueldo>Sueldo(entre 500 y 5000):</label>
    <input type="text" name="sueldo" id="sueldo" /></br>
		
	<label for=irpf>Retencion IRPF(f 0,30):</label>
    <input type="text" name="irpf" id="irpf" /></br>
		
    <p class="der">
    <input type="submit" value="Enviar" /> 
    <input type="reset" value="Borrar" name="Reset" /></p>
  </fieldset>
</form>



</body>
</html>	
EOFORM;
		
echo $formulario;

?>

<?php 
//validacion de nombre
if(isset($_GET['nombre'])){
	
	$nombre=strip_tags(htmlspecialchars(trim($_GET['nombre'])));
	if (!ctype_alpha(str_replace(" ","",$nombre))){
		echo $nombre, ' no puede ser numerico</br>';				
		}
	else {
		echo $nombre, ' es un nombre correcto</br>';
		}
}
else 
	echo 'No se ha introducido nada';

if(isset($_GET['edad'])){
	
	$edad=strip_tags(htmlspecialchars(trim($_GET['edad'])));
	if (filter_var($edad,FILTER_VALIDATE_INT)){
		echo $edad, ' edad correcta</br>';				
		}
	else {
		echo $edad, ' es una edad incorrecta</br>';
		}
}
else 
	echo 'No se ha introducido nada';

if(isset($_GET['email'])){

	$email=strip_tags(htmlspecialchars(trim($_GET['email'])));
	if (filter_var($email,FILTER_VALIDATE_EMAIL)){
		echo $email, ' email correcto</br>';
	}
	else {
		echo $email, ' es un email incorrecto</br>';
	}
}
else
	echo 'No se ha introducido nada';

if(isset($_GET['sueldo'])){

	$sueldo=strip_tags(htmlspecialchars(trim($_GET['sueldo'])));
	if (filter_var($sueldo,FILTER_VALIDATE_INT)){
		echo $sueldo, ' sueldo correcto</br>';
	}
	else {
		echo $sueldo, ' es un sueldo incorrecto</br>';
	}
}
else
	echo 'No se ha introducido nada';

if(isset($_GET['irpf'])){

	$irpf=strip_tags(htmlspecialchars(trim($_GET['irpf'])));
	if (filter_var($irpf,FILTER_VALIDATE_FLOAT)){
		echo $irpf, ' irpf correcto</br>';
	}
	else {
		echo $irpf, ' es un irpf incorrecto</br>';
	}
}
else
	echo 'No se ha introducido nada';


?>