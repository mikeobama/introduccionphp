<?php

$formulario='
		
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Comprobación de datos (Formulario). Comprobación de datos. PHP.
  Bartolomé Sintes Marco</title>
  <meta name="generator" content="amaya 8.7.1, see http://www.w3.org/Amaya/" />
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>

<form action="comprobacion_datos_1.php" method="get">
  <h1>Comprobación de datos</h1>
  <fieldset>
    <legend>Formulario</legend>
    <p>Escriba cualquier cosa en el campo siguiente excepto código html
    (&lt;...&gt;) para analizarlo con distintas funciones de PHP:</p>

    <p><strong>Dato:</strong> <input type="text" name="dato" /></p>

    <p class="der">
    <input type="submit" value="Enviar" /> 
    <input type="reset" value="Borrar" name="Reset" /></p>
  </fieldset>
</form>



</body>
</html>	';
		
echo $formulario;

?>

<?php 
//validacion de datos
if(isset($_GET['dato'])){
	echo 'Se ha introducido un dato</br>';
	$dato=strip_tags(htmlspecialchars(trim($_GET['dato'])));
	if (is_numeric($dato)){
		echo $dato, ' es un numero</br>';
		if (filter_var($dato,FILTER_VALIDATE_INT)) {
			echo $dato, ' es un entero';
		}
		elseif (filter_var($dato,FILTER_VALIDATE_FLOAT))
			echo $dato, ' es un float';			
		}
	else {
		echo $dato, ' no es un numero</br>';
		if (filter_var($dato,FILTER_VALIDATE_EMAIL))
			echo $dato, ' es un email</br>';
		else 
			echo $dato, ' no es un email</br>';
		}
}
else 
	echo 'No se ha introducido nada'

?>